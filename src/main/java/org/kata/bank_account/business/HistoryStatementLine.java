package org.kata.bank_account.business;

public record HistoryStatementLine(Operation operation, Balance balance) {
}