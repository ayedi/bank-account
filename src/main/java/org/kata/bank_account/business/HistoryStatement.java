package org.kata.bank_account.business;

import java.util.ArrayList;
import java.util.List;

import org.kata.bank_account.infrastructure.HistoryStatementPrinter;

public class HistoryStatement {

	private List<HistoryStatementLine> historyStatementLines = new ArrayList<>();

    public void add(HistoryStatementLine historyStatementLine) {
        historyStatementLines.add(historyStatementLine);
    }

    public void print(HistoryStatementPrinter historyStatementPrinter) {
    	historyStatementPrinter.print(this);
    }

    public List<HistoryStatementLine> historyStatementLines() {
        return historyStatementLines;
    }

    public void add(Operation operation, Balance balance) {
    	historyStatementLines.add(new HistoryStatementLine(operation, balance));
    }
}
