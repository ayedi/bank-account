package org.kata.bank_account.business;

import java.math.BigDecimal;
import java.util.Objects;

public record Amount(BigDecimal value) {

	public Amount {
		Objects.requireNonNull(value);
		if (value.compareTo(BigDecimal.ZERO) == -1) {
			throw new IllegalArgumentException("Amount can not be negative");
		}
	}
}
