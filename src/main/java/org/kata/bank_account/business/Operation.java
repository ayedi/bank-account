package org.kata.bank_account.business;

import java.time.LocalDateTime;

public record Operation(OperationType operationType, Amount amount, LocalDateTime date) {

}
