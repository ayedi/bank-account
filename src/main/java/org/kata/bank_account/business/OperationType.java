package org.kata.bank_account.business;

public enum OperationType {
	DEPOSIT,
	WITHDRAWAL
}
