package org.kata.bank_account.business;

import java.time.Clock;
import java.time.LocalDateTime;

import org.kata.bank_account.infrastructure.HistoryStatementPrinter;

public class Account {
	
    private Balance balance;
    private HistoryStatement historyStatement = new HistoryStatement();
    private Clock clock;

    public Account(Balance balance) {
        this.balance = balance;
        this.clock = Clock.systemDefaultZone();
    }
    
    public Account(Balance balance, Clock clock) {
        this.balance = balance;
        this.clock = clock;
    }

    public Balance balance() {
        return balance;
    }
    
    public void deposit(Amount amount) {
        balance = balance.add(amount);
        historyStatement.add(new Operation(OperationType.DEPOSIT, amount, LocalDateTime.now(clock)), balance);
    }
    
    public void withdrawal(Amount amount) {
        balance = balance.subtract(amount);
        historyStatement.add(new Operation(OperationType.WITHDRAWAL, amount, LocalDateTime.now(clock)), balance);
    }
    
    public void print(HistoryStatementPrinter historyStatementPrinter) {
    	historyStatement.print(historyStatementPrinter);
    }
}