package org.kata.bank_account.infrastructure;

import org.kata.bank_account.business.HistoryStatement;

public interface HistoryStatementPrinter {
	
    void print(HistoryStatement historyStatement);
}
