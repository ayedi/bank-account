package org.kata.bank_account.infrastructure;

import java.time.format.DateTimeFormatter;

import org.kata.bank_account.business.HistoryStatement;

public class ConsoleHistoryStatementPrinter implements HistoryStatementPrinter{
	
	private String header = "OPERATION | DATE | AMOUNT | BALANCE \n";
	private String historyLineFormat = "%s | %s | %s | %s \n";

	@Override
	public void print(HistoryStatement historyStatement) {
		StringBuilder stringBuilder = new StringBuilder(header);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		historyStatement.historyStatementLines().forEach(historyStatementLine -> {
			String formattedhistoryLine = String.format(
					historyLineFormat,
					historyStatementLine.operation().operationType(),
					historyStatementLine.operation().date().format(formatter),
					historyStatementLine.operation().amount().value(),
					historyStatementLine.balance().value());
			stringBuilder.append(formattedhistoryLine);
		});
		System.out.println(stringBuilder);
	}
	
}
