package org.kata.bank_account.infrastructure;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.kata.bank_account.business.Account;
import org.kata.bank_account.business.Amount;
import org.kata.bank_account.business.Balance;
import org.kata.bank_account.business.OperationType;

public class ConsoleHistoryStatementPrinterTest {
	
	Clock fixedClock = Clock.fixed(Instant.now(), ZoneId.systemDefault());
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
	
	private static ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
	private String header = "OPERATION | DATE | AMOUNT | BALANCE \n";
	private String historyLineFormat = "%s | %s | %s | %s \n";
	private static final BigDecimal ONE_HUNDRED = BigDecimal.valueOf(100);
	
	@BeforeAll
    public static void fixtureSetUp(){
        System.setOut(new PrintStream(outputStream));
    }
	
	@Test
    void should_print_deposit_and_withdrawal_statement(){

        Account account = new Account(new Balance(BigDecimal.ZERO), fixedClock);
        account.deposit(new Amount(ONE_HUNDRED));
        account.withdrawal(new Amount(ONE_HUNDRED));
        account.print(new ConsoleHistoryStatementPrinter());

        String expectedDate = LocalDateTime.now(fixedClock).format(formatter);
        StringBuilder stringBuilder = new StringBuilder(header);
        stringBuilder.append(String.format(historyLineFormat, OperationType.DEPOSIT, expectedDate,	ONE_HUNDRED, ONE_HUNDRED));
        stringBuilder.append(String.format(historyLineFormat, OperationType.WITHDRAWAL, expectedDate, ONE_HUNDRED, BigDecimal.ZERO));
        
        Assertions.assertEquals(stringBuilder.toString().strip(), outputStream.toString().strip());
    }
	
	@AfterEach
    public void tearDown(){
		outputStream = new ByteArrayOutputStream();
    }

    @AfterAll
    public static void fixtureTearDown() {
        System.setOut(System.out);
    }

}