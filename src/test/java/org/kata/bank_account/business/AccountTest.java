package org.kata.bank_account.business;

import java.math.BigDecimal;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.kata.bank_account.infrastructure.HistoryStatementPrinter;

public class AccountTest {
	
	private final static BigDecimal ONE_HUNDRED = BigDecimal.valueOf(100);
	private final static BigDecimal MINUS_ONE_HUNDRED = BigDecimal.valueOf(-100);
	
	Clock fixedClock = Clock.fixed(Instant.now(), ZoneId.systemDefault());

    @Test
    void should_create_account_with_zero_balance(){
        Account account = new Account(new Balance(BigDecimal.ZERO));
        Assertions.assertEquals(new Balance(BigDecimal.ZERO),account.balance());
    }
    
    @Test
    void should_create_account_with_100_balance(){
        Account account = new Account(new Balance(ONE_HUNDRED));
        Assertions.assertEquals(new Balance(ONE_HUNDRED),account.balance());
    }
    
    @Test
    void should_deposit_amount_100_in_account_with_zero_balance(){
        Account account = new Account(new Balance(BigDecimal.ZERO));
        Amount amount = new Amount(ONE_HUNDRED);
        account.deposit(amount);
        Assertions.assertEquals(new Balance(ONE_HUNDRED), account.balance());
    }
    
    @Test
    void should_withdrawal_amount_100_from_account_with_zero_balance(){
        Account account = new Account(new Balance(BigDecimal.ZERO));
        Amount amount = new Amount(ONE_HUNDRED);
        account.withdrawal(amount);
        Assertions.assertEquals(new Balance(MINUS_ONE_HUNDRED), account.balance());
    }
    
    @Test
    void should_withdrawal_amount_100_from_account_with_100_balance(){
        Account account = new Account(new Balance(ONE_HUNDRED));
        Amount amount = new Amount(ONE_HUNDRED);
        account.withdrawal(amount);
        Assertions.assertEquals(new Balance(BigDecimal.ZERO), account.balance());
    }
    
    @Test
    void should_print_deposit_history_statement_line(){
    	FakeHistoryStatementPrinter HistoryStatementPrinter = new FakeHistoryStatementPrinter();
        Account account = new Account(new Balance(BigDecimal.ZERO), fixedClock);
        account.deposit(new Amount(ONE_HUNDRED));
        account.print(HistoryStatementPrinter);
        Assertions.assertTrue(HistoryStatementPrinter.lines.contains(new HistoryStatementLine(
        		new Operation(OperationType.DEPOSIT, new Amount(ONE_HUNDRED), LocalDateTime.now(fixedClock)), account.balance())));
    }
    
    @Test
    void should_print_withdrawal_history_statement_line(){
    	FakeHistoryStatementPrinter HistoryStatementPrinter = new FakeHistoryStatementPrinter();
        Account account = new Account(new Balance(ONE_HUNDRED), fixedClock);
        account.withdrawal(new Amount(ONE_HUNDRED));
        account.print(HistoryStatementPrinter);
        Assertions.assertTrue(HistoryStatementPrinter.lines.contains(new HistoryStatementLine(
        		new Operation(OperationType.WITHDRAWAL, new Amount(ONE_HUNDRED), LocalDateTime.now(fixedClock)), account.balance())));
    }
    
    private static class FakeHistoryStatementPrinter implements HistoryStatementPrinter {

        private final List<HistoryStatementLine> lines = new ArrayList<>();

        @Override
        public void print(HistoryStatement historyStatement) {
            lines.addAll(historyStatement.historyStatementLines());
        }
    }

}
