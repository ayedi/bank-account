package org.kata.bank_account.business;

import java.math.BigDecimal;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class AmountTest {
	
	private static final BigDecimal MINUS_ONE_HUNDRED = BigDecimal.valueOf(-100);

	@Test
    void should_throw_exception_when_amount_value_is_negative(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new Amount(MINUS_ONE_HUNDRED);
        });
    }

}