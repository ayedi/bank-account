package org.kata.bank_account.business;

import java.math.BigDecimal;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BalanceTest {
	
	private static final BigDecimal ONE_HUNDRED = BigDecimal.valueOf(100);
	private static final BigDecimal MINUS_ONE_HUNDRED = BigDecimal.valueOf(-100);

	@Test
	void should_create_balance_100_value(){
		Balance balance = new Balance(ONE_HUNDRED);
		Assertions.assertEquals(ONE_HUNDRED, balance.value());
	}

	@Test
	void should_add_100_to_balance_with_zero_value(){
		Balance balance = new Balance(BigDecimal.ZERO);
		Balance newBalance = balance.add(new Amount(ONE_HUNDRED));
		Assertions.assertEquals(ONE_HUNDRED, newBalance.value());
	}
	
	@Test
    void should_subtract_100_from_balance_with_zero_value(){
        Balance balance = new Balance(BigDecimal.ZERO);
        Balance newBalance = balance.subtract(new Amount(ONE_HUNDRED));
        Assertions.assertEquals(MINUS_ONE_HUNDRED, newBalance.value());
    }

}